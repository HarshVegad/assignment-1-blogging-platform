<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

     //User Routes

    Route::get('upload', \App\Http\Controllers\uploadcontroller::class.'@index');
    Route::post('store', \App\Http\Controllers\uploadcontroller::class.'@store')->name('upload.store');

    Route::get('post/{post?}', \App\Http\Controllers\User\PostController::class.'@post')->name('post');
    Route::get('/', \App\Http\Controllers\User\HomeController::class.'@index2');
    Route::get('/post/tag/{tag}', \App\Http\Controllers\User\HomeController::class.'@tag')->name('tag');
    Route::get('/post/category/{category}', \App\Http\Controllers\User\HomeController::class.'@category')->name('category');
    Route::get('/user/home', \App\Http\Controllers\User\HomeController::class.'@index')->name('user.home');
    Route::get('/user/about', \App\Http\Controllers\User\HomeController::class.'@about')->name('user.about');
    Route::get('/user/samplepost', \App\Http\Controllers\User\HomeController::class.'@samplepost')->name('user.samplepost');
    Route::get('/user/contact', \App\Http\Controllers\User\HomeController::class.'@contact')->name('user.contact');

    //Admin  Routes

     Route::get('admin/home', \App\Http\Controllers\Admin\HomeController::class.'@index')->name('admin.home');

    //Admin  Routes-Post

     Route::get('admin/post', '\App\Http\Controllers\Admin\PostController@index')->name('post.index');
     Route::get('admin/postcreate', \App\Http\Controllers\Admin\PostController::class.'@create')->name('post.postcreate');
     Route::post('admin/poststore', \App\Http\Controllers\Admin\PostController::class.'@store')->name('post.store');
     Route::any('admin/postedit/{id}', \App\Http\Controllers\Admin\PostController::class.'@edit')->name('post.edit');
     Route::post('admin/postdestroy/{id}', \App\Http\Controllers\Admin\PostController::class.'@destroy')->name('post.destroy');
     Route::post('admin/postupdate/{id}', \App\Http\Controllers\Admin\PostController::class.'@update')->name('post.update');

    //Admin  Routes-category

    Route::get('admin/category', \App\Http\Controllers\Admin\CategoryController::class.'@index')->name('category.index');
    Route::get('admin/categorycreate', \App\Http\Controllers\Admin\CategoryController::class.'@create')->name('category.categorycreate');
    Route::post('admin/categorystore', \App\Http\Controllers\Admin\CategoryController::class.'@store')->name('category.store');
    Route::any('admin/categoryedit/{id}', \App\Http\Controllers\Admin\CategoryController::class.'@edit')->name('category.edit');
    Route::any('admin/categorydestroy/{id}', '\App\Http\Controllers\Admin\CategoryController@destroy')->name('category.destroy');
    Route::any('admin/categoryupdate/{id}', '\App\Http\Controllers\Admin\CategoryController@update')->name('category.update');

    //Admin  Routes-user

     Route::get('admin/user', \App\Http\Controllers\Admin\UserController::class.'@index')->name('user.index');
     Route::get('admin/usercreate', \App\Http\Controllers\Admin\UserController::class.'@create')->name('user.usercreate');
     Route::post('admin/userstore', \App\Http\Controllers\Admin\UserController::class.'@store')->name('user.store');
     Route::any('admin/useredit/{id}', \App\Http\Controllers\Admin\UserController::class.'@edit')->name('user.edit');
     Route::any('admin/userdestroy/{id}', '\App\Http\Controllers\Admin\UserController@destroy')->name('user.destroy');
     Route::any('admin/userupdate/{id}', '\App\Http\Controllers\Admin\UserController@update')->name('user.update');

    //Admin  Routes-tag

     Route::get('admin/tag', \App\Http\Controllers\Admin\TagController::class.'@index')->name('tag.index');
     Route::get('admin/tagcreate', \App\Http\Controllers\Admin\TagController::class.'@create')->name('tag.tagcreate');
     Route::post('admin/tagstore', \App\Http\Controllers\Admin\TagController::class.'@store')->name('tag.store');
     Route::any('admin/tagedit/{id}', \App\Http\Controllers\Admin\TagController::class.'@edit')->name('tag.edit');
     Route::any('admin/tagdestroy/{id}', '\App\Http\Controllers\Admin\TagController@destroy')->name('tag.destroy');
     Route::any('admin/tagupdate/{id}', '\App\Http\Controllers\Admin\TagController@update')->name('tag.update');

     //Admin  Routes-role

      Route::get('admin/role', \App\Http\Controllers\Admin\RoleController::class.'@index')->name('role.index');
     Route::get('admin/rolecreate', \App\Http\Controllers\Admin\RoleController::class.'@create')->name('role.rolecreate');
     Route::post('admin/rolestore', \App\Http\Controllers\Admin\RoleController::class.'@store')->name('role.store');
     Route::any('admin/roleedit/{id}', \App\Http\Controllers\Admin\RoleController::class.'@edit')->name('role.edit');
     Route::any('admin/roledestroy/{id}', '\App\Http\Controllers\Admin\RoleController@destroy')->name('role.destroy');
     Route::any('admin/roleupdate/{id}', '\App\Http\Controllers\Admin\RoleController@update')->name('role.update');

    //Admin  Routes-permission

     Route::get('admin/permission', \App\Http\Controllers\Admin\PermissionController::class.'@index')->name('permission.index');
     Route::get('admin/permissioncreate', \App\Http\Controllers\Admin\PermissionController::class.'@create')->name('permission.permissioncreate');
     Route::post('admin/permissionstore', \App\Http\Controllers\Admin\PermissionController::class.'@store')->name('permission.store');
     Route::any('admin/permissionedit/{id}', \App\Http\Controllers\Admin\PermissionController::class.'@edit')->name('permission.edit');
     Route::any('adminpermissiondestroy/{id}', '\App\Http\Controllers\Admin\PermissionController@destroy')->name('permission.destroy');
     Route::any('admin/permissionupdate/{id}', '\App\Http\Controllers\Admin\PermissionController@update')->name('permission.update');

    //Admin auth route

    Auth::routes();

    Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
    Route::get('/admin-login', [App\Http\Controllers\Admin\Auth\LoginController::class, 'showLoginForm'])->name('admin.login');
    Route::post('/admin-login', [App\Http\Controllers\Admin\Auth\LoginController::class, 'login']);
