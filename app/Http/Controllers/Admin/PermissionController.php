<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\admin\Permission;
use Illuminate\Http\Request;

class PermissionController extends Controller
{
    public function index()
    {
        $permissions = Permission::all();

        return view('admin.permission.show', compact('permissions'));
    }

    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    public function create()
    {
        return view('admin.permission.permission');
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|max:50|unique:permissions',
            'for' => 'required',
        ]);
        $tag = new Permission();
        $tag->name = $request->name;
        $tag->for = $request->for;

        $tag->save();

        return redirect(route('permission.index'))->with('message', 'Permission is Succesfully added');
    }

    public function edit($id)
    {
        $permission = Permission::where('id', $id)->first();

        return view('admin.permission.edit', compact('permission'));
    }

    public function destroy($id)
    {
        Permission::where('id', $id)->delete();

        return redirect()->back()->with('message', 'Permission is Succesfully deleted');
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required',
                      'for' => 'required',
        ]);
        $tag = Permission::find($id);
        $tag->name = $request->name;
        $tag->for = $request->for;

        $tag->save();

        return redirect(route('permission.index'))->with('message', 'Permission is Succesfully updated');
    }
}
