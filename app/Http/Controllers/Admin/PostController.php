<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\user\category;
use App\Models\user\post;
use App\Models\user\tag;
//use App\Models\user\category_post;
use Illuminate\Http\Request;

class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $post = post::all();

        return view('admin.post.show', compact('post'));
    }

    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $tags = tag::all();

        $categories = category::all();

        return view('admin.post.post', compact('tags', 'categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
                'title' => 'required',
                'subtitle' => 'required',
                'slug' => 'required',
                'body' => 'required',
                'image' => 'required',
            ]);

        if ($request->hasFile('image')) {
            $imagename = $request->image->store('public');
        }
        $post = new post();
        $post->image = $imagename;

        $post->title = $request->title;
        $post->subtitle = $request->subtitle;
        $post->slug = $request->slug;
        $post->status = $request->status;

        $post->tagid = $request->tags;
        $post->categoryid = $request->categories;
        $post->body = $request->body;

        $post->save();

        return redirect(route('post.index'))->with('message', 'Post is Succesfully added');
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $post = post::where('id', $id)->first();
        $tags = tag::all();

        $categories = category::all();

        return view('admin.post.edit', compact('post', 'tags', 'categories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'title' => 'required',
            'subtitle' => 'required',
            'slug' => 'required',
            'body' => 'required',
          //  'image' => 'required',
        ]);

        $post = post::find($id);

        if ($request->hasFile('image')) {
            $imagename = $request->image->store('public');
            $post->image = $imagename;
        }

        $post->title = $request->title;
        $post->subtitle = $request->subtitle;
        $post->slug = $request->slug;
        $post->status = $request->status;

        $post->tagid = $request->tags;
        $post->categoryid = $request->categories;
        $post->body = $request->body;

        $post->save();

        return redirect(route('post.index'))->with('message', 'Post is Succesfully updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        post::where('id', $id)->delete();

        return redirect()->back()->with('message', 'Post is Succesfully deleted');
    }
}
