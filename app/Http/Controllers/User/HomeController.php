<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Models\user\category;
use App\Models\user\post;
use App\Models\user\tag;

class HomeController extends Controller
{
    public function index()
    {
        // $posts = Post::where('status', 1)->paginate(5);

        $posts = Post::leftjoin('categories', 'posts.categoryid', '=', 'categories.id')->leftjoin('tags', 'tags.id', '=', 'posts.tagid')
        ->select('posts.*', 'tags.name as tagname', 'categories.name as categoryname')->where('posts.status', 1)->paginate(8);

        return view('user.blogs', compact('posts'));
    }

    public function index2()
    {
        // $posts = Post::all();

        return redirect(route('login'));
    }

    public function about()
    {
        return view('user.about');
    }

    public function samplepost()
    {
        return view('user.samplepost');
    }

    public function contact()
    {
        return view('user.contact');
    }

    public function tag()
    {
        $posts = Post::where('status', 1)->paginate(5);
        // $posts = Post::all();

        return view('user.blogs', compact('posts'));
    }

    public function category()
    {
        $posts = Post::where('status', 1)->paginate(5);
        // $posts = Post::all();

        return view('user.blogs', compact('posts'));
    }
}
