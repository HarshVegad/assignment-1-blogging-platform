@extends('admin.layouts.app')
@section('main-content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Text Editors</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Text Editors</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>
    <div class="container-fluid">
      <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Titles</h3>
              </div>  @if ($errors->any())

              <div class="alert alert-danger">
                <button type="button" class="close" data-dismiss="alert">×</button>
           @foreach ($errors->all() as $error)
           <li>{{ $error }}</li>
           @endforeach
           </div>
              @endif
              <!-- /.card-header -->
              <!-- form start -->
              <form role="form" action="{{ route('role.store')}}" method="post" >{{csrf_field()}}
                <div class="card-body">
                  <div class="form-group">
                    <label for="name">Role Title</label>
                    <input type="text" class="form-control" name="name" id="name" placeholder="Role Title">
                  </div>
                  <div class="row">
                      <div class="col-lg-4">

                      <label class="">Posts Permissions</label>
                      @foreach($permissions as $permissionrole)
                      @if($permissionrole->for == 'post')
                       <div class="checkbox">
                        <label><input type="checkbox" name="permission[]" value="{{$permissionrole->id}}">{{$permissionrole->name}}</label>
                     </div>
                     @endif
                     @endforeach


                </div>

                <div class="col-lg-4">
                    <label class="">User Permissions</label>

                    @foreach($permissions as $permissionrole)
                    @if($permissionrole->for == 'user')
                     <div class="checkbox">
                      <label><input type="checkbox"  name="permission[]" value="{{$permissionrole->id}}">{{$permissionrole->name}}</label>
                   </div>
                   @endif
                   @endforeach

              </div>
              <div class="col-lg-4">
                <label class="">User Permissions</label>

                @foreach($permissions as $permissionrole)
                @if($permissionrole->for == 'other')
                 <div class="checkbox">
                  <label><input type="checkbox"   name="permission[]" value="{{$permissionrole->id}}">{{$permissionrole->name}}</label>
               </div>
               @endif
               @endforeach

          </div>
                  </div>

                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Submit</button>
                  <a href="{{ route('role.index')}}" class="btn btn-warning">Back</a>

                </div>
              </form>
            </div></div>
    <!-- Main content -->

    <!-- /.content -->
  </div>

@endsection
