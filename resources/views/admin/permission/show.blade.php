@extends('admin.layouts.app')

@section('main-content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Permission    <a class="btn btn-success"href="{{ route('permission.permissioncreate')}}">Add New</a> </h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Permission </li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
        @if ($message = Session::get('message'))
        <div class="alert alert-success alert-block">
          <button type="button" class="close" data-dismiss="alert">×</button>
          <strong>{{ $message }}</strong>
        </div>
      @endif
      <!-- Default box -->
      <div class="card">
        <div class="card-header">
          <h3 class="card-title">DataTable with default features</h3>

        </div>

        <!-- /.card-header -->
        <div class="card-body">
          <table id="example1" class="table table-bordered table-striped">
            <thead>
            <tr>
              <th>Sr No</th>
              <th>Permission Name</th>
              <th>Permission For</th>

              <th>Edit </th>
              <th>Delete </th>

                </tr>
            </thead>
            <tbody>
                <?php $i=0;?>
            @foreach  (  $permissions as $tag)
                    <?php $i++;?>
            <tr>
              <td><?php echo $i;?></td>
              <td>{{$tag->name}}</td>
              <td>{{$tag->for}}</td>

              </td>
            <td><a href="{{ route('permission.edit', $tag->id) }}"><i class="fas fa-edit"></i></a></td>
            <form method="post" id="delete-form-{{ $tag->id }}" action="{{ route('permission.destroy',$tag->id) }}" style="display: none">
            {{csrf_field()}}

            </form>
            <td><a onClick="
                if(confirm('Are you sure you want to delete this ?'))
                {
                    event.preventDefault();
                    document.getElementById('delete-form-{{ $tag->id }}').submit();
                }


                else
                {
                    event.preventDefault();
                }" href=""><i class="fas fa-trash"></i></a></td>

            </tr>
            @endforeach

            </tbody>
            <tfoot>
            <tr>
                <th>Sr No</th>
                <th>Permission Name</th>
                <th>Permission For</th>
                <th>Edit </th>
                <th>Delete </th>

            </tr>
            </tfoot>
          </table>
        </div>
        <!-- /.card-body -->
      </div>
      <!-- /.card -->

    </section>
    <!-- /.content -->
  </div>

@endsection

