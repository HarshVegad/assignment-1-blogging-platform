@extends('admin.layouts.app')
@section('main-content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Text Editors</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Text Editors</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>
    <div class="container-fluid">
      <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Titles</h3>
              </div>  @if ($errors->any())

              <div class="alert alert-danger">
                <button type="button" class="close" data-dismiss="alert">×</button>
           @foreach ($errors->all() as $error)
           <li>{{ $error }}</li>
           @endforeach
           </div>
              @endif
              <!-- /.card-header -->
              <!-- form start -->
              <form role="form" action="{{ route('permission.update',$permission->id)}}" method="post" >{{csrf_field()}}{{ method_field('PUT')}}
                <div class="card-body">
                  <div class="form-group">
                    <label for="name">Permission </label>
                    <input type="text" class="form-control" value="{{$permission->name}}" name="name" id="name" placeholder="Permission ">
                  </div>

                  <div class="form-group">
                    <label for="name">Permission For </label>
                    <select id="for" name="for" class="form-control">
                        <option selected disabled>Select Permission For</option>
                        <option  @if ($permission->for == "user")  selected  @endif   value="user">User</option>
                        <option  @if ($permission->for == "post") selected  @endif value="post">Post</option>

                        <option  @if ($permission->for == "other") selected  @endif value="other">Other</option>

                    </select>
                </div>




                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Submit</button>
                  <a href="{{ route('permission.index')}}" class="btn btn-warning">Back</a>

                </div>
              </form>
            </div></div>
    <!-- Main content -->

    <!-- /.content -->
  </div>

@endsection
