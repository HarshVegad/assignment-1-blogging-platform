@extends('admin.layouts.app')
@section('main-content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Text Editors</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Text Editors</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>
    <div class="container-fluid">
      <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Add Admin</h3>
              </div>  @if ($errors->any())

              <div class="alert alert-danger">
                <button type="button" class="close" data-dismiss="alert">×</button>
           @foreach ($errors->all() as $error)
           <li>{{ $error }}</li>
           @endforeach
           </div>
              @endif
              <!-- /.card-header -->
              <!-- form start -->
              <form role="form" action="{{ route('user.update',$user->id) }}" method="post" >{{csrf_field()}}{{method_field('PUT')}}
                <div class="card-body">
                  <div class="form-group">
                    <label for="name">User Name</label>
                    <input type="text" class="form-control" name="name" id="name" value=
                    "@if (old('name')) {{old('name')}} @else {{$user->name}} @endif
                    "placeholder="User Name">
                  </div>

                  <div class="form-group">
                    <label for="slug">Email</label>
                    <input type="text" class="form-control" name="email"   value="@if (old('email')) {{old('email')}} @else {{$user->email}} @endif
                    " id="email" placeholder="Email">
                  </div>

                  <div class="form-group">
                    <label for="slug">Phone Number</label>
                    <input type="text" class="form-control"  value="@if (old('phone')) {{old('phone')}} @else {{$user->phone}} @endif
                    " name="phone" id="phone" placeholder="Phone">
                  </div>

{{--
<div class="form-group">
    <label for="slug">Status </label>
    <div class="checkbox">

        <label><input type="checkbox" @if( old('status')==1 || $user->status==1 )checked @endif name="status" value="1">Status</label>
        </div>
         </div> --}}


<div class="form-group col-lg-12">
    <label for="slug">Assign Role</label>
<div class="row">

    @foreach ($roles as $role)
    <div class="col-lg-3">

        <div class="checkbox">
            <label><input type="checkbox" name="role[]" value="{{$role->id}}"
                @foreach($user->roles as $userrole)
                @if($userrole->id == $role->id)
                checked
                @endif
                @endforeach
                >{{$role->name}}</label>


            </div>
            </div>
            @endforeach

</div>

  </div>



                </div>

                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Submit</button>
                  <a href="{{ route('user.index')}}" class="btn btn-warning">Back</a>

                </div>
              </form>
            </div></div>
    <!-- Main content -->

    <!-- /.content -->
  </div>

@endsection
