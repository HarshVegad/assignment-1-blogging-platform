@extends('admin.layouts.app')
@section('main-content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Text Editors</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Text Editors</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>
    <div class="container-fluid">
      <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Titles</h3>
              </div>   @if ($errors->any())

              <div class="alert alert-danger">
                <button type="button" class="close" data-dismiss="alert">×</button>
           @foreach ($errors->all() as $error)
           <li>{{ $error }}</li>
           @endforeach
           </div>
              @endif
              <form role="form" action="{{ route('post.store')}}" method="post" enctype="multipart/form-data">{{csrf_field()}}
                <div class="card-body">
                  <div class="form-group">
                    <label for="label">Post Title</label>
                    <input type="text" class="form-control" name="title" id="title" placeholder="Post Title">
                  </div>
                  <div class="form-group">
                    <label for="subtitle">Post Sub Title</label>
                    <input type="text" class="form-control" name="subtitle" id="subtitle" placeholder="Post Sub Title">
                  </div>
                  <div class="form-group">
                    <label for="slug">Post Slug</label>
                    <input type="text" class="form-control" name="slug" id="slug" placeholder="Post Slug">
                  </div>

                  <div class="form-group">
                    <label for="image">File input</label>
                    <div class="input-group">
                      <div class="custom-file">
                        <input type="file" name="image" class="custom-file-input" id="image">
                        <label class="custom-file-label" for="image">Choose file</label>
                      </div>

                    </div>
                  </div>

                  <div class="form-group" >
                    <label>Select Tags</label>
                    <select name="tags" class="form-control select2 " style="width: 100%;" data-select2-id="1" tabindex="-1" aria-hidden="true">
                @foreach ($tags as $tag)
                <option value="{{ $tag->id }}">{{ $tag->name }}</option>
                @endforeach
                    </select>
                  </div>

                  <div class="form-group" >
                    <label>Select Category</label>
                    <select name="categories" class="form-control select2 " style="width: 100%;" data-select2-id="1" tabindex="-1" aria-hidden="true">
                                            @foreach ($categories as $category)
                    <option value="{{ $category->id }}">{{ $category->name }}</option>
                    @endforeach
                  </select>

                  </div>

                  <div class="form-check">
                    <input type="checkbox"  class="form-check-input" name="status" value="1" id="status">
                    <label class="form-check-label" for="publish">Publish</label>
                  </div>


                </div>
                <!-- /.card-body -->



            </div></div>
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-md-12">
          <div class="card card-outline card-info">
            <div class="card-header">
              <h3 class="card-title">
                Write Post Here
                <small>Simple and fast</small>
              </h3>
              <!-- tools box -->
              <div class="card-tools">
                <button type="button" class="btn btn-tool btn-sm" data-card-widget="collapse" data-toggle="tooltip"
                        title="Collapse">
                  <i class="fas fa-minus"></i></button>

              </div>
              <!-- /. tools -->
            </div>
            <!-- /.card-header -->
            <div class="card-body pad">
              <div class="mb-3">
                <textarea class="textarea" placeholder="Place some text here" name="body"
                          style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"></textarea>
              </div>

            </div>
          </div>
        </div>
        <!-- /.col-->
      </div>
      <!-- ./row -->
    </section>
    <div class="card-footer">
        <button type="submit" class="btn btn-primary">Submit</button>
        <a href="{{ route('post.index')}}" class="btn btn-warning">Back</a>

      </div>
    </form>
    <!-- /.content -->
  </div>

@endsection
