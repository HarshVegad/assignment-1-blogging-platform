<html lang="en">
    <head>
        <meta charset="utf-8">
        <title>Demo</title>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
    </head>
    <body>
        <br>
        <div>
            <center><h1>Upload a file</h1></center>
            <form action="{{ route('upload.store')}}"  enctype="multipart/form-data" method="post">{{csrf_field()}}
                <input type="file" name="image">
                <br>
                <input type="submit" value="Upload">

        </div>
    </body>
</html>
