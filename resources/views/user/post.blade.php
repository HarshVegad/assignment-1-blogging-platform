@extends('user/app')
@section('bg-img',Storage::disk('local')->url($post->image))
@section('title',$post->title)
@section('subheading',$post->subtitle)
@section('main-content')
<article>
    <div class="container">
      <div class="row">
        <div class="col-lg-8 col-md-10 mx-auto">
           <small class="">Created At {{$post->created_at->diffForHumans()}}</small>

            @foreach ($post->categories as $category)
            <a href="">   <small >
                 {{$category->name }}
                </small></a>
            @endforeach


                {!! htmlspecialchars_decode( $post->body) !!}
                <h3>Tag Cloud</h3>
  @foreach ($post->tags as $category)
          <a href="">  <small> {{$category->name }}</small></a>
            @endforeach
        </div>

      </div>
    </div>
  </article>

  <hr>
@endsection
