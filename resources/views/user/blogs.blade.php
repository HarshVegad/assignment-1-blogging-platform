@extends('user/app')
@section('bg-img',asset('user/img/post-bg.jpg'))
@section('title','Demo')
@section('subheading','Harsh Vegad')


@section('main-content')

<div class="container">
    <div class="row">
      <div class="col-lg-8 col-md-10 mx-auto">
        <div class="post-preview">
            @foreach($posts as $post)
            <a href="{{ route('post',$post->slug) }}">
            <h5 >
               {{ $post->title}}
            </h5>
            <h5 >
              {{  $post->subtitle }}
            </h5>
          </a>
          <h5 >Category -
            {{  $post->categoryname }}</h5>
          <h5>Tag -
            {{  $post->tagname }}</h5>

          <p class="post-meta">Posted
            <a href="#"></a> {{$post->created_at->diffForHumans()}}
            </p>
        </div>
        @endforeach


        <!-- Pager -->
        <ul class="clearfix">
            <li class="next">{{$posts->links()}}
          <a  href="#">Older Posts &rarr;</a>
          </li></ul>
        </div>
      </div>
    </div>
  </div>
@endsection

