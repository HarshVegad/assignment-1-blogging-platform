<!DOCTYPE html>
<html lang="en">

@include('user/layout/head')

<body>

  @include('user/layout/header')

  <!-- Main Content -->
    @section('main-content')

    @show

  <hr>

  <!-- Footer -->
  @include('user/layout/footer')

</body>

</html>
