-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Oct 26, 2020 at 07:03 AM
-- Server version: 10.4.8-MariaDB
-- PHP Version: 7.3.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `blog`
--

-- --------------------------------------------------------

--
-- Table structure for table `admins`
--

CREATE TABLE `admins` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `admins`
--

INSERT INTO `admins` (`id`, `name`, `email`, `password`, `phone`, `status`, `created_at`, `updated_at`) VALUES
(6, 'Demo User', 'harsh21@gmail.com', '$2y$10$rhaGKh18Ie6ZzTuHDefyyekFqElHlZxSvzpcU2/4mVCCPTdKfcqd.', '9824868148', 0, '2020-10-22 05:00:35', '2020-10-25 23:13:52'),
(7, 'Harsh Tester', 'harshvegad33@gmail.com', '$2y$10$UGEE3x3ao7RZHb1EYMox/uq2hi7IYp9WcUuWyhEzwd6.mQUeQEJj.', '7600640425', 0, '2020-10-23 06:40:23', '2020-10-23 06:40:37');

-- --------------------------------------------------------

--
-- Table structure for table `admin_role`
--

CREATE TABLE `admin_role` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `admin_id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `admin_role`
--

INSERT INTO `admin_role` (`id`, `admin_id`, `role_id`, `created_at`, `updated_at`) VALUES
(1, 4, 13, NULL, NULL),
(2, 4, 14, NULL, NULL),
(5, 4, 15, NULL, NULL),
(8, 6, 13, NULL, NULL),
(10, 6, 15, NULL, NULL),
(11, 4, 18, NULL, NULL),
(12, 7, 14, NULL, NULL),
(13, 7, 15, NULL, NULL),
(14, 7, 13, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `name`, `slug`, `created_at`, `updated_at`) VALUES
(1, 'Mobiles', 'Redmi', '2020-10-16 07:27:28', '2020-10-25 23:19:00'),
(3, 'Entrepreneur', 'Project', '2020-10-19 04:30:16', '2020-10-25 23:20:14'),
(8, 'Devices', 'HardwareDevices', '2020-10-23 06:17:40', '2020-10-25 23:20:51');

-- --------------------------------------------------------

--
-- Table structure for table `category_posts`
--

CREATE TABLE `category_posts` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `post_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `category_posts`
--

INSERT INTO `category_posts` (`id`, `post_id`, `category_id`, `created_at`, `updated_at`) VALUES
(1, 3, 3, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uuid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1),
(4, '2020_10_14_095809_create_posts_table', 1),
(5, '2020_10_14_100438_create_tags_table', 1),
(6, '2020_10_14_100545_create_categories_table', 1),
(7, '2020_10_14_100739_create_post_tags_table', 2),
(8, '2020_10_14_100921_create_admins_table', 2),
(9, '2020_10_14_101142_create_roles_table', 2),
(10, '2020_10_14_101223_create_admin_roles_table', 2),
(11, '2020_10_14_103739_create_category_posts_table', 2),
(12, '2020_10_21_070402_create_permissions_table', 3),
(13, '2020_10_21_072714_create_permissions_table', 4);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `password_resets`
--

INSERT INTO `password_resets` (`email`, `token`, `created_at`) VALUES
('harshvegad33@gmail.com', '$2y$10$hhgv9x8ttv9a7ew7G6M61eisc9FsNBnyHq7R7ijZmqDREm5KnxtU.', '2020-10-22 01:57:39');

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE `permissions` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `for` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permissions`
--

INSERT INTO `permissions` (`id`, `name`, `for`, `created_at`, `updated_at`) VALUES
(4, 'Post Update', 'post', '2020-10-21 05:01:55', '2020-10-23 06:27:43'),
(5, 'Post-Delete', 'post', '2020-10-21 05:02:10', '2020-10-21 05:02:10'),
(6, 'User-Create', 'user', '2020-10-21 05:02:32', '2020-10-21 05:02:32'),
(7, 'User-Update', 'user', '2020-10-21 05:02:47', '2020-10-21 05:02:47'),
(8, 'User-Delete', 'user', '2020-10-21 05:02:59', '2020-10-21 05:02:59'),
(9, 'Post Publish', 'post', '2020-10-21 05:03:19', '2020-10-21 05:03:19'),
(10, 'Tag CRUD', 'other', '2020-10-21 05:03:45', '2020-10-21 05:03:45'),
(12, 'Demo Permission', 'post', '2020-10-22 05:30:42', '2020-10-22 05:30:42'),
(13, 'Post Create', 'post', '2020-10-22 06:07:20', '2020-10-22 06:07:20'),
(16, 'Category CRUD', 'other', '2020-10-23 06:28:52', '2020-10-23 06:28:52');

-- --------------------------------------------------------

--
-- Table structure for table `permission_role`
--

CREATE TABLE `permission_role` (
  `role_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permission_role`
--

INSERT INTO `permission_role` (`role_id`, `permission_id`) VALUES
(13, 4),
(13, 10),
(14, 9),
(16, 3),
(16, 9),
(17, 6),
(17, 7),
(17, 8),
(18, 4),
(18, 5),
(13, 5),
(15, 4),
(15, 5),
(15, 10),
(15, 16),
(19, 5);

-- --------------------------------------------------------

--
-- Table structure for table `posts`
--

CREATE TABLE `posts` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(256) COLLATE utf8mb4_unicode_ci NOT NULL,
  `categoryid` int(11) NOT NULL,
  `tagid` int(11) NOT NULL,
  `subtitle` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `body` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` int(10) DEFAULT NULL,
  `posted_by` int(11) DEFAULT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `posts`
--

INSERT INTO `posts` (`id`, `title`, `categoryid`, `tagid`, `subtitle`, `slug`, `body`, `status`, `posted_by`, `image`, `created_at`, `updated_at`) VALUES
(22, 'XIAOMI’S REVENUE EXCEEDS RMB200 BILLION IN 2019', 1, 1, 'All Segments Grow Despite Headwinds, Adjusted Net Profit of RMB 11.5 billion', 'FinancialHighlights', '<p style=\"margin-right: 0px; margin-bottom: 1.25em; margin-left: 0px; padding: 0px; font-size: 17px; line-height: 1.5; letter-spacing: -0.022em; color: rgb(102, 102, 102); font-family: ProximaNova, sans-serif;\">Hong Kong, March 31, 2020 — Xiaomi Corporation (“Xiaomi” or the “Group”; Stock Code:1810), an internet company with smartphone and smart hardware connected by an Internet of Things(“IoT”) platform at its core, today announced its audited consolidated results for the year ended December 31, 2019.</p><p style=\"margin-right: 0px; margin-bottom: 1.25em; margin-left: 0px; padding: 0px; font-size: 17px; line-height: 1.5; letter-spacing: -0.022em; color: rgb(102, 102, 102); font-family: ProximaNova, sans-serif;\">In 2019, Xiaomi achieved solid growth across all business segments. The Group’s total revenue exceeded RMB200 billion for the first time, reaching RMB205.8 billion, up 17.7% year-on-year (“YoY”), representing a 9-year CAGR of 112%. Adjusted net profit was RMB11.5 billion, up 34.8% YoY.</p><p style=\"margin-right: 0px; margin-bottom: 1.25em; margin-left: 0px; padding: 0px; font-size: 17px; line-height: 1.5; letter-spacing: -0.022em; color: rgb(102, 102, 102); font-family: ProximaNova, sans-serif;\">In the fourth quarter of 2019, Xiaomi’s total revenue grew by 27.1% to RMB56.5 billion. Adjusted net profit was RMB2.3 billion, increasing by 26.5% YoY.</p><p style=\"margin-right: 0px; margin-bottom: 1.25em; margin-left: 0px; padding: 0px; font-size: 17px; line-height: 1.5; letter-spacing: -0.022em; color: rgb(102, 102, 102); font-family: ProximaNova, sans-serif;\">2019 Financial Highlights</p><ul style=\"padding-left: 50px; color: rgb(102, 102, 102); font-family: ProximaNova, sans-serif;\"><li style=\"margin-bottom: 8px;\">Total revenue was approximately RMB205.84 billion, up 17.7% YoY beat market consensus;</li><li style=\"margin-bottom: 8px;\">Gross profit was approximately RMB28.55 billion, up 28.7% YoY;</li><li style=\"margin-bottom: 8px;\">Non-IFRS adjusted net profit was RMB11.53 billion, up 34.8% YoY, beat market consensus;</li><li style=\"margin-bottom: 8px;\">Earnings per share were RMB0.0423;</li><li style=\"margin-bottom: 8px;\">The aggregate amount of cash resources was RMB66 billion.</li></ul><p style=\"margin-right: 0px; margin-bottom: 1.25em; margin-left: 0px; padding: 0px; font-size: 17px; line-height: 1.5; letter-spacing: -0.022em; color: rgb(102, 102, 102); font-family: ProximaNova, sans-serif;\">4Q2019 Financial Highlights</p><ul style=\"padding-left: 50px; color: rgb(102, 102, 102); font-family: ProximaNova, sans-serif;\"><li style=\"margin-bottom: 8px;\">Total revenue at historical high of approximately RMB56.47 billion, up 27.1% YoY, the best performing quarter of the year;</li><li style=\"margin-bottom: 8px;\">Gross profit was approximately RMB7.84 billion, up 38.5% YoY;</li><li style=\"margin-bottom: 8px;\">Non-IFRS adjusted net profit was RMB2.34 billion, up 26.5% YoY.</li></ul>', 1, NULL, 'public/jlAgtAkw91VCphpJkteV3lWGd0DaN0fTuo5oHXyr.png', '2020-10-25 23:27:18', '2020-10-26 00:08:42');

-- --------------------------------------------------------

--
-- Table structure for table `post_tags`
--

CREATE TABLE `post_tags` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `post_id` int(11) NOT NULL,
  `tag_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `post_tags`
--

INSERT INTO `post_tags` (`id`, `post_id`, `tag_id`, `created_at`, `updated_at`) VALUES
(1, 3, 3, NULL, NULL),
(3, 3, 4, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `created_at`, `updated_at`) VALUES
(13, 'Writer', '2020-10-21 06:04:48', '2020-10-21 06:04:48'),
(14, 'Publisher', '2020-10-21 06:05:03', '2020-10-21 06:05:03'),
(15, 'Editor', '2020-10-21 06:05:46', '2020-10-21 06:05:46');

-- --------------------------------------------------------

--
-- Table structure for table `tags`
--

CREATE TABLE `tags` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tags`
--

INSERT INTO `tags` (`id`, `name`, `slug`, `created_at`, `updated_at`) VALUES
(1, 'Gadgets', 'Mobile', '2020-10-16 07:18:29', '2020-10-25 23:16:16'),
(3, 'Startup', 'Company', '2020-10-19 05:37:09', '2020-10-25 23:16:52'),
(12, 'Hardware', 'ComputerDevices', '2020-10-23 06:20:55', '2020-10-25 23:17:46');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Harsh Vegad', 'harshvegad33@gmail.com', NULL, '$2y$10$kS0KcCbKtvVQN1RQLoyMUueW0yfDdh5gFuMs636q444vg5eTG6eMi', '7spm6tOscE7Wx15noJgI07xMWUKkwjWVllunZwLP3WTZFgFcdEtuvSnv1I6m', '2020-10-20 06:22:48', '2020-10-20 06:22:48'),
(2, 'Harsh Tester', 'harshvegad116@gmail.com', NULL, '$2y$10$S4B4VpkPOGz3UnTbBWBUseLG3Ln/e4uFTZ2yRCliqFy2A5pBYgaDy', NULL, '2020-10-20 07:00:54', '2020-10-20 07:00:54'),
(3, 'Demo User', 'demouser3@gmail.com', NULL, '$2y$10$nWmT0ynM9kxNGKo8MmbF2usZheKXZLeidkqDa60ouBcRbrRVynTce', NULL, '2020-10-22 01:26:07', '2020-10-22 01:26:07');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admins`
--
ALTER TABLE `admins`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `admins_email_unique` (`email`);

--
-- Indexes for table `admin_role`
--
ALTER TABLE `admin_role`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `category_posts`
--
ALTER TABLE `category_posts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `posts`
--
ALTER TABLE `posts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `post_tags`
--
ALTER TABLE `post_tags`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tags`
--
ALTER TABLE `tags`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admins`
--
ALTER TABLE `admins`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `admin_role`
--
ALTER TABLE `admin_role`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `category_posts`
--
ALTER TABLE `category_posts`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `posts`
--
ALTER TABLE `posts`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT for table `post_tags`
--
ALTER TABLE `post_tags`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `tags`
--
ALTER TABLE `tags`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
